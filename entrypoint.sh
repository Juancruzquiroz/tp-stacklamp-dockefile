#!/bin/bash
set -e

# Si existe el fichero index.html, vaciamos directorio /var/www/html


if [ -f /var/www/html/index.html ]; then
    rm -r /var/www/html/*
fi

# Fijarse si el directorio '/etc/apache2' esta vacio

if [ -z "$(ls -A /etc/apache2)" ]; then

    # Si el directorio esta vacio, se copian ficheros desde '/app/apache2'"
    cp -r /app/apache2/* /etc/apache2
fi

# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'

sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /app/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /app/apache2/sites-available/default.conf
cp /app/apache2/sites-available/default.conf /etc/apache2/sites-available/default.conf

# Activar sitio

a2ensite default.conf


# Bases de datos

# Iniciar mysql server para poder ejecutar los comandos posteriores
/etc/init.d/mysql start

# Esperamos 5 segundos para que cargue mysql
sleep 5

# Contraseña de root
# en bash ls comparaciones se hacen de dos formas
# para enteros '-eq'
# para cadena de caracteres '=='
# -f /fichero comprueba su existencia, si exite devuelve true
if [ ! -f /app/mysql.configured ]; then
    if [ $MYSQL_USER_PASSWORD == "123456" ];then

        RPASS=$((10000 + $RANDOM %30000))

        mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$RPASS';"

        touch /app/mysql.configured

    else
        mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
        touch /app/mysql.configured
    fi
fi
# Crear base de datos

if [ ! -f /app/mysql.database.configured ]; then
    if [ $MYSQL_DB_NAME == "wordpress" ];then
    if [ $MYSQL_DB_NAME == "stacklamp" ];then

        mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
        mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
        touch /app/mysql.database.configured
    else
        mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
        mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
        touch /app/mysql.database.configured
    fi
fi


# Inicio de servicios

apachectl -D FOREGROUND


exec "$@"

